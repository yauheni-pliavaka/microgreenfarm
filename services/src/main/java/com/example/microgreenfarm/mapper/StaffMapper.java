package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.StaffDto;
import com.example.microgreenfarm.entity.Staff;
import org.mapstruct.Mapper;

@Mapper
public interface StaffMapper {

    Staff map(StaffDto dto);

    StaffDto mapToDto(Staff entity);
}
