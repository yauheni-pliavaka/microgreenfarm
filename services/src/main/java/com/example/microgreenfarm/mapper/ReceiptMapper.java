package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.ReceiptDto;
import com.example.microgreenfarm.entity.Receipt;
import org.mapstruct.Mapper;

@Mapper
public interface ReceiptMapper {

    Receipt map(ReceiptDto dto);

    ReceiptDto mapToDto(Receipt entity);
}
