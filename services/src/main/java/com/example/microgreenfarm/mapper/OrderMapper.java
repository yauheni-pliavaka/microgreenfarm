package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.OrderDto;
import com.example.microgreenfarm.entity.Order;
import org.mapstruct.Mapper;

@Mapper
public interface OrderMapper {

    Order map(OrderDto dto);

    OrderDto mapToDto(Order entity);
}
