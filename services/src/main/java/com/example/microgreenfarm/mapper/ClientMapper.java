package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.ClientDto;
import com.example.microgreenfarm.entity.Client;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(uses = {ContactDetailsMapper.class, ChildMapper.class})
public interface ClientMapper {

    Client map(ClientDto dto);

    ClientDto mapToDto(Client entity);
}
