package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.NotesDto;
import com.example.microgreenfarm.entity.Notes;
import org.mapstruct.Mapper;

@Mapper
public interface NotesMapper {

    Notes map(NotesDto dto);

    NotesDto mapToDto(Notes entity);
}
