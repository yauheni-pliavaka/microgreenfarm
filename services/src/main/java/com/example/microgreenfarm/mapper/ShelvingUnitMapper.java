package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.ShelvingUnitDto;
import com.example.microgreenfarm.entity.ShelvingUnit;
import org.mapstruct.Mapper;

@Mapper(uses = MicrogreenMapper.class)
public interface ShelvingUnitMapper {

    ShelvingUnit map(ShelvingUnitDto dto);

    ShelvingUnitDto mapToDto(ShelvingUnit entity);
}
