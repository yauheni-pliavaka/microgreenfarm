package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.ItemDto;
import com.example.microgreenfarm.entity.Item;
import org.mapstruct.Mapper;

@Mapper
public interface ItemMapper {

    Item map(ItemDto dto);

    ItemDto mapToDto(Item entity);
}
