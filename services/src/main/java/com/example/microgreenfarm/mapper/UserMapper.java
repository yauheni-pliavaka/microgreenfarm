package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.UserDto;
import com.example.microgreenfarm.entity.User;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {

    User map(UserDto dto);

    UserDto mapToDto(User entity);
}
