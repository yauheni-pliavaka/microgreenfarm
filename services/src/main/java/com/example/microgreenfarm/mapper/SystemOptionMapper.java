package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.SystemOptionDto;
import com.example.system.entity.SystemOptionEntity;
import org.mapstruct.Mapper;

@Mapper
public interface SystemOptionMapper {

    SystemOptionDto mapToDto(SystemOptionEntity entity);
}
