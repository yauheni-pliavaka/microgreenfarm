package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.ChildDto;
import com.example.microgreenfarm.entity.Child;
import org.mapstruct.Mapper;

@Mapper
public interface ChildMapper {

    Child map(ChildDto dto);

    ChildDto mapToDto(Child entity);
}
