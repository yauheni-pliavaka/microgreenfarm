package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.PermissionDto;
import com.example.microgreenfarm.entity.Permission;
import org.mapstruct.Mapper;

@Mapper
public interface PermissionMapper {

    Permission map(PermissionDto dto);

    PermissionDto mapToDto(Permission entity);
}
