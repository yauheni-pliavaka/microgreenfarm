package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.GrowerDto;
import com.example.microgreenfarm.entity.Grower;
import org.mapstruct.Mapper;

@Mapper
public interface GrowerMapper {

    Grower map(GrowerDto dto);

    GrowerDto mapToDto(Grower entity);
}
