package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.ProviderDto;
import com.example.microgreenfarm.entity.Provider;
import org.mapstruct.Mapper;

@Mapper
public interface ProviderMapper {

    Provider map(ProviderDto dto);

    ProviderDto mapToDto(Provider entity);
}
