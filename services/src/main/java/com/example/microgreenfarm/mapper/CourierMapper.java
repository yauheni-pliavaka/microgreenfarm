package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.CourierDto;
import com.example.microgreenfarm.entity.Courier;
import org.mapstruct.Mapper;

@Mapper
public interface CourierMapper {

    Courier map(CourierDto dto);

    CourierDto mapToDto(Courier entity);
}
