package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.ContactDetailsDto;
import com.example.microgreenfarm.entity.ContactDetails;
import org.mapstruct.Mapper;

@Mapper
public interface ContactDetailsMapper {

    ContactDetailsDto mapToDto(ContactDetails details);

    ContactDetails map(ContactDetailsDto dto);
}
