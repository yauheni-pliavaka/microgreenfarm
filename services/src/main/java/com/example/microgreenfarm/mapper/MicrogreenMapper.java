package com.example.microgreenfarm.mapper;

import com.example.microgreenfarm.dto.MicrogreenDto;
import com.example.microgreenfarm.entity.Microgreen;
import org.mapstruct.Mapper;

@Mapper(uses = ShelvingUnitMapper.class)
public interface MicrogreenMapper {

    Microgreen map(MicrogreenDto dto);

    MicrogreenDto mapToDto(Microgreen entity);
}
