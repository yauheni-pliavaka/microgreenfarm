package com.example.microgreenfarm.services;

import java.util.Collection;

public interface CrudService<D, ID> {

    D findById(ID id);

    void save(D dto);

    Collection<D> findAll();

    void delete(ID id);
}
