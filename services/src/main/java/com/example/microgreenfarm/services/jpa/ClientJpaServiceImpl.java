package com.example.microgreenfarm.services.jpa;

import com.example.microgreenfarm.dto.ClientDto;
import com.example.microgreenfarm.entity.Child;
import com.example.microgreenfarm.entity.Client;
import com.example.microgreenfarm.mapper.ClientMapper;
import com.example.microgreenfarm.repository.ClientRepository;
import com.example.microgreenfarm.services.ClientService;
import com.example.microgreenfarm.services.config.JpaImplementation;
import com.example.microgreenfarm.specification.SearchableRepository;
import com.example.microgreenfarm.specification.SearchableService;
import com.example.microgreenfarm.specification.filter.ClientFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true) //будем читать все данные в транзакции, но не будем блочить данные на запись
@JpaImplementation
public class ClientJpaServiceImpl extends AbstractJpaService<ClientDto, Client, Long> implements ClientService, SearchableService<Client> {

    private final ClientRepository clientRepository;
    private final ClientMapper mapper;

    @Override
    public JpaRepository<Client, Long> getRepository() {
        return clientRepository;
    }

    @Override
    public SearchableRepository<Client, ?> getSearchRepository() {
        return clientRepository;
    }

    @Override
    public ClientDto mapToDto(Client entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Client mapToEntity(ClientDto dto) {
        return mapper.map(dto);
    }

    @Override
    public Collection<ClientDto> findByName(String fullName) {
        return clientRepository.findByFullName(fullName).stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public void updateFullName(Long id, String fullName) {
        super.findById(id).setFullName(fullName);
    }

    //вложенная транзакция
    //@Transactional(propagation = Propagation.REQUIRES_NEW)
    //Propagation указывает на то, что будем делать, если метод уже вызван в другой транзакции
    public void updateClientName(Long clientId, String clientFullName) {
        Client client = clientRepository.findById(clientId).orElseThrow();
        client.setFullName(clientFullName);

//        clientRepository.save(client); писать не нужно, потому что в транзакции все изменения сразу попадают в
//                                       персистенс контекст и изменения при выходе из транзакции коммитаются в БД
    }

    public void readClientAndChildren() {
        List<Client> clients = clientRepository.findAll();
        for (Client client : clients) {
            log.warn("Client name {}", client.getFullName());
            for (Child child : client.getChildren()) {
                log.warn("Child name: {}", child.getFullName());
            }
        }
    }

    @Override
    public List<ClientDto> findClient(ClientDto clientDto) {
        ExampleMatcher caseInsensitiveExMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Client> example = Example.of(mapper.map(clientDto), caseInsensitiveExMatcher);
        //из dto преобразуем все в энтити, получаем example, часть полей могут быть null
        return clientRepository.findAll(example).stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public Page<ClientDto> search(ClientFilter clientFilter) {
        return searchPage(clientFilter).map(mapper::mapToDto);
    }

    @Override
    public List<Client> searchByName(String clientFirstName) {
        return clientRepository.findByFirstName(clientFirstName);
    }
}
