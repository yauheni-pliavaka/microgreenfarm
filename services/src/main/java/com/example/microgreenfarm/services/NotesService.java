package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.NotesDto;

public interface NotesService extends CrudService<NotesDto, Long> {
}
