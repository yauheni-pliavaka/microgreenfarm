package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.ReceiptDto;

public interface ReceiptService extends CrudService<ReceiptDto, Long> {
}
