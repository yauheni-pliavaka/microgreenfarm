package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.dto.ReceiptDto;
import com.example.microgreenfarm.entity.Receipt;
import com.example.microgreenfarm.mapper.ReceiptMapper;
import com.example.microgreenfarm.repository.ReceiptRepository;
import com.example.microgreenfarm.services.ReceiptService;
import com.example.microgreenfarm.services.jpa.AbstractJpaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@RequiredArgsConstructor
@Service
public class ReceiptServiceImpl extends AbstractJpaService<ReceiptDto, Receipt, Long> implements ReceiptService {

    private final ReceiptRepository receiptRepository;
    private final ReceiptMapper mapper;

    @Override
    public ReceiptDto findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void save(ReceiptDto entity) {
        receiptRepository.save(mapToEntity(entity));
    }

    @Override
    public Collection<ReceiptDto> findAll() {
        return super.findAll();
    }

    @Override
    public void delete(Long id) {
        receiptRepository.deleteById(id);
    }

    public ReceiptDto mapToDto(Receipt entity) {
        return mapper.mapToDto(entity);
    }

    public Receipt mapToEntity(ReceiptDto dto) {
        return mapper.map(dto);
    }

    @Override
    public JpaRepository<Receipt, Long> getRepository() {
        return receiptRepository;
    }
}
