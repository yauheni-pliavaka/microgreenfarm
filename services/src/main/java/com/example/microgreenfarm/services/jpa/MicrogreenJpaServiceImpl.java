package com.example.microgreenfarm.services.jpa;


import com.example.microgreenfarm.dto.MicrogreenDto;
import com.example.microgreenfarm.entity.Microgreen;
import com.example.microgreenfarm.enums.MicrogreenType;
import com.example.microgreenfarm.entity.ShelvingUnit;
import com.example.microgreenfarm.mapper.MicrogreenMapper;
import com.example.microgreenfarm.repository.MicrogreenRepository;
import com.example.microgreenfarm.services.MicrogreenService;
import com.example.microgreenfarm.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@RequiredArgsConstructor
@JpaImplementation
public class MicrogreenJpaServiceImpl extends AbstractJpaService<MicrogreenDto, Microgreen, Long> implements MicrogreenService {

    private final MicrogreenRepository microgreenRepository;
    private final MicrogreenMapper mapper;

    @Override
    public JpaRepository<Microgreen, Long> getRepository() {
        return microgreenRepository;
    }

    @Override
    public Microgreen findByShelvingUnit(ShelvingUnit shelvingUnit) {
        throw new UnsupportedOperationException();  //заглушка, чтобы не было NPE
    }

    @Override
    public void updateCostPrice(Long microgreenId, BigDecimal costPrice) {
        throw new UnsupportedOperationException();  //заглушка, чтобы не было NPE
    }

    @Override
    public Microgreen findByHarvestDate(MicrogreenType microgreenType, OffsetDateTime harvestDate) {
        throw new UnsupportedOperationException();  //заглушка, чтобы не было NPE
    }

    @Override
    public MicrogreenDto mapToDto(Microgreen entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Microgreen mapToEntity(MicrogreenDto dto) {
        return mapper.map(dto);
    }
}