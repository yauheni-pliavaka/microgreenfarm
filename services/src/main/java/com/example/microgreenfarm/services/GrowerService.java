package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.GrowerDto;

public interface GrowerService extends CrudService<GrowerDto, Long> {
}
