package com.example.microgreenfarm.services.impl;

import com.example.listener.AuditLogService;
import com.example.microgreenfarm.aspect.LogExecutionTime;
import com.example.microgreenfarm.dto.SystemLogDto;
import com.example.microgreenfarm.mapper.SystemLogMapper;
import com.example.microgreenfarm.services.SystemLogService;
import com.example.system.entity.SystemLogEntity;
import com.example.system.repository.SystemLogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class SystemLogServiceImpl implements SystemLogService, AuditLogService {

    @Lazy  //разрываем циклическую зависимость
    @Autowired
    private SystemLogRepository systemLogRepository;

    private final SystemLogMapper mapper;

    @LogExecutionTime
    @Override
    public List<SystemLogDto> getAll() {
        return mapper.mapListToDto(systemLogRepository.findAll());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, transactionManager = "systemTransactionManager")
    @Override
    public void createLogs(String activity, String message) {
        systemLogRepository.save(SystemLogEntity.builder()
                .activity(activity)
                .message(message)
                .build());
    }
}
