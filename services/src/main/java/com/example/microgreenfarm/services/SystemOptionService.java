package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.SystemOptionDto;

import java.util.List;

public interface SystemOptionService {

    List<SystemOptionDto> getAll();

    void save(SystemOptionDto option);

    SystemOptionDto getById(String optionKey);
}
