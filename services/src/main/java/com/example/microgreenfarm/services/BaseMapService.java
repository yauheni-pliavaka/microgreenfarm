package com.example.microgreenfarm.services;

import java.util.Map;

public interface BaseMapService<D, ID> {

    Map<ID, D> getResources();
}
