package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.ClientDto;
import com.example.microgreenfarm.entity.Client;
import com.example.microgreenfarm.specification.filter.ClientFilter;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;

public interface ClientService extends CrudService<ClientDto, Long> {

    Collection<ClientDto> findByName(String name);

    void updateFullName(Long id, String newFullName);

    default List<ClientDto> findClient(ClientDto clientDto) {
        throw new UnsupportedOperationException();
    }

    default Page<ClientDto> search(ClientFilter clientFilter) {
        throw new UnsupportedOperationException();
    }

    default List<Client> searchByName(String clientFirstName) {
        throw new UnsupportedOperationException();
    }
}
