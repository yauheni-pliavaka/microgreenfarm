package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.dto.GrowerDto;
import com.example.microgreenfarm.entity.Grower;
import com.example.microgreenfarm.mapper.GrowerMapper;
import com.example.microgreenfarm.repository.GrowerRepository;
import com.example.microgreenfarm.services.GrowerService;
import com.example.microgreenfarm.services.jpa.AbstractJpaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class GrowerServiceImpl extends AbstractJpaService<GrowerDto, Grower, Long> implements GrowerService {

    private final GrowerRepository growerRepository;
    private final GrowerMapper mapper;

    @Override
    public GrowerDto mapToDto(Grower entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Grower mapToEntity(GrowerDto dto) {
        return mapper.map(dto);
    }

    @Override
    public JpaRepository<Grower, Long> getRepository() {
        return growerRepository;
    }
}
