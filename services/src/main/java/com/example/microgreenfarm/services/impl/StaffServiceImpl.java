package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.dto.StaffDto;
import com.example.microgreenfarm.entity.Staff;
import com.example.microgreenfarm.mapper.StaffMapper;
import com.example.microgreenfarm.repository.StaffRepository;
import com.example.microgreenfarm.services.StaffService;
import com.example.microgreenfarm.services.jpa.AbstractJpaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class StaffServiceImpl extends AbstractJpaService<StaffDto, Staff, Long> implements StaffService {

    private final StaffRepository staffRepository;
    private final StaffMapper mapper;

    @Override
    public StaffDto mapToDto(Staff entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Staff mapToEntity(StaffDto dto) {
        return mapper.map(dto);
    }

    @Override
    public JpaRepository<Staff, Long> getRepository() {
        return staffRepository;
    }
}
