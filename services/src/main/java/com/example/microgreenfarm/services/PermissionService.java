package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.PermissionDto;

import java.util.Collection;

public interface PermissionService {

    Collection<PermissionDto> findAll();
}
