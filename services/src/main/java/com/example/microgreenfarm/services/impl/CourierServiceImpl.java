package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.dto.CourierDto;
import com.example.microgreenfarm.entity.Courier;
import com.example.microgreenfarm.mapper.CourierMapper;
import com.example.microgreenfarm.repository.CourierRepository;
import com.example.microgreenfarm.services.CourierService;
import com.example.microgreenfarm.services.jpa.AbstractJpaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CourierServiceImpl extends AbstractJpaService<CourierDto, Courier, Long> implements CourierService {

    private final CourierRepository courierRepository;
    private final CourierMapper mapper;

    @Override
    public CourierDto mapToDto(Courier entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Courier mapToEntity(CourierDto dto) {
        return mapper.map(dto);
    }

    @Override
    public JpaRepository<Courier, Long> getRepository() {
        return courierRepository;
    }
}
