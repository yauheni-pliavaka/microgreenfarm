package com.example.microgreenfarm.services.map;

import com.example.microgreenfarm.dto.ClientDto;
import com.example.microgreenfarm.services.ClientService;
import com.example.microgreenfarm.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class ClientMapService extends AbstractMapService<ClientDto, Long> implements ClientService {

    private static final Map<Long, ClientDto> resource = new HashMap<>();

    @Override
    public Map<Long, ClientDto> getResources() {
        return resource;
    }

    @Override
    public Collection<ClientDto> findByName(String name) {
        return null;
    }

    @Override
    public void updateFullName(Long id, String newFullName) {
    }
}
