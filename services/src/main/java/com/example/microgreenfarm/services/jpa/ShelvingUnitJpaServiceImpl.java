package com.example.microgreenfarm.services.jpa;

import com.example.microgreenfarm.dto.ShelvingUnitDto;
import com.example.microgreenfarm.entity.BaseEntity;
import com.example.microgreenfarm.entity.ShelvingUnit;
import com.example.microgreenfarm.enums.MicrogreenType;
import com.example.microgreenfarm.mapper.ShelvingUnitMapper;
import com.example.microgreenfarm.services.ShelvingUnitService;
import com.example.microgreenfarm.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

@RequiredArgsConstructor
@JpaImplementation
public class ShelvingUnitJpaServiceImpl extends AbstractJpaService<ShelvingUnitDto, ShelvingUnit, Long> implements ShelvingUnitService {

    private final ShelvingUnitMapper mapper;

    @Override
    public JpaRepository<ShelvingUnit, Long> getRepository() {
        throw new UnsupportedOperationException();  //заглушка, чтобы не было NPE
    }

    @Override
    public Collection<ShelvingUnitDto> findByMicrogreenType(MicrogreenType microgreenType) {
        throw new UnsupportedOperationException();  //заглушка, чтобы не было NPE
    }

    @Override
    public ShelvingUnitDto mapToDto(ShelvingUnit entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public ShelvingUnit mapToEntity(ShelvingUnitDto dto) {
        return mapper.map(dto);
    }
}
