package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.dto.PermissionDto;
import com.example.microgreenfarm.entity.Permission;
import com.example.microgreenfarm.mapper.PermissionMapper;
import com.example.microgreenfarm.repository.PermissionRepository;
import com.example.microgreenfarm.services.PermissionService;
import com.example.microgreenfarm.services.jpa.AbstractJpaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@RequiredArgsConstructor
@Service
public class PermissionServiceImpl extends AbstractJpaService<PermissionDto, Permission, Long> implements PermissionService {

    private final PermissionRepository permissionRepository;
    private final PermissionMapper mapper;

    @Override
    public PermissionDto findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void save(PermissionDto entity) {
        permissionRepository.save(mapToEntity(entity));
    }

    @Override
    public Collection<PermissionDto> findAll() {
        return super.findAll();
    }

    @Override
    public void delete(Long id) {
        permissionRepository.deleteById(id);
    }

    public PermissionDto mapToDto(Permission entity) {
        return mapper.mapToDto(entity);
    }

    public Permission mapToEntity(PermissionDto dto) {
        return mapper.map(dto);
    }

    @Override
    public JpaRepository<Permission, Long> getRepository() {
        return permissionRepository;
    }
}
