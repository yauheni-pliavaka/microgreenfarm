package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.ShelvingUnitDto;
import com.example.microgreenfarm.enums.MicrogreenType;

import java.util.Collection;

public interface ShelvingUnitService extends CrudService<ShelvingUnitDto, Long> {

    Collection<ShelvingUnitDto> findByMicrogreenType(MicrogreenType microgreenType);
}
