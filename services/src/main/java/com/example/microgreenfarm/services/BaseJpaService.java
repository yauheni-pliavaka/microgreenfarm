package com.example.microgreenfarm.services;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseJpaService<D, ID> {

    JpaRepository<D, ID> getRepository();
}
