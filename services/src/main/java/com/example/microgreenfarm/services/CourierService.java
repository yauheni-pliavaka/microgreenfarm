package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.CourierDto;

public interface CourierService extends CrudService<CourierDto, Long> {

}
