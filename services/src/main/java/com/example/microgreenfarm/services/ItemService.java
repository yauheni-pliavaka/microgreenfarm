package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.ItemDto;
import com.example.microgreenfarm.entity.ItemPK;

public interface ItemService extends CrudService<ItemDto, ItemPK> {
}
