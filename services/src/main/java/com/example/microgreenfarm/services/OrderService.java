package com.example.microgreenfarm.services;

public interface OrderService {

    void cancelOrder(Long id);

    void markOrderAsPayed(Long id);
}
