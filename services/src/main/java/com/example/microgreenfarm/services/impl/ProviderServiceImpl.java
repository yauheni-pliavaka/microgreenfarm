package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.dto.ProviderDto;
import com.example.microgreenfarm.entity.Provider;
import com.example.microgreenfarm.mapper.ProviderMapper;
import com.example.microgreenfarm.repository.ProviderRepository;
import com.example.microgreenfarm.services.ProviderService;
import com.example.microgreenfarm.services.jpa.AbstractJpaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ProviderServiceImpl extends AbstractJpaService<ProviderDto, Provider, Long> implements ProviderService {

    private final ProviderRepository providerRepository;
    private final ProviderMapper mapper;

    @Override
    public ProviderDto mapToDto(Provider entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Provider mapToEntity(ProviderDto dto) {
        return mapper.map(dto);
    }

    @Override
    public JpaRepository<Provider, Long> getRepository() {
        return providerRepository;
    }
}
