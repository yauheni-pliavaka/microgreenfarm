package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.aspect.ActivityLog;
import com.example.microgreenfarm.aspect.LogExecutionTime;
import com.example.microgreenfarm.entity.Order;
import com.example.microgreenfarm.enums.OrderStatus;
import com.example.microgreenfarm.repository.OrderRepository;
import com.example.microgreenfarm.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @LogExecutionTime
    @ActivityLog(activity = "order_cancel", value = "order {id} cancelled")
    @Override
    public void cancelOrder(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(NoSuchElementException::new); //to do custom exception
        if (order.getStatus().equals(OrderStatus.PAYED)) {
            throw new UnsupportedOperationException("cannot cancel payed order");
        } else {
            order.setStatus(OrderStatus.CANCELLED);
        }
        orderRepository.save(order);
    }

    @LogExecutionTime
    @ActivityLog(activity = "order_payed", value = "order {id} payed")
    @Override
    public void markOrderAsPayed(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(NoSuchElementException::new); //to do custom exception
        if (order.getStatus().equals(OrderStatus.CANCELLED)) {
            throw new UnsupportedOperationException("cannot mark cancelled order as payed");
        } else {
            order.setStatus(OrderStatus.PAYED);
        }
        orderRepository.save(order);
    }
}
