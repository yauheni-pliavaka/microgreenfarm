package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.dto.UserDto;
import com.example.microgreenfarm.entity.User;
import com.example.microgreenfarm.mapper.UserMapper;
import com.example.microgreenfarm.repository.UserRepository;
import com.example.microgreenfarm.services.UserService;
import com.example.microgreenfarm.services.jpa.AbstractJpaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@RequiredArgsConstructor
@Service
public class UserServiceImpl extends AbstractJpaService<UserDto, User, Long> implements UserService {

    private final UserRepository userRepository;
    private final UserMapper mapper;

    @Override
    public UserDto findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void save(UserDto entity) {
        userRepository.save(mapToEntity(entity));
    }

    @Override
    public Collection<UserDto> findAll() {
        return super.findAll();
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    public UserDto mapToDto(User entity) {
        return mapper.mapToDto(entity);
    }

    public User mapToEntity(UserDto dto) {
        return mapper.map(dto);
    }

    @Override
    public JpaRepository<User, Long> getRepository() {
        return userRepository;
    }
}
