package com.example.microgreenfarm.services.map;


import com.example.microgreenfarm.services.BaseMapService;
import com.example.microgreenfarm.services.CrudService;

import java.util.Collection;

public abstract class AbstractMapService<D, ID> implements CrudService<D, ID>, BaseMapService<D, ID> {

    @Override
    public D findById(ID id) {
        return getResources().get(id);
    }

    @Override
    public void save(D entity) {
//        getResources().put(entity.getId(), entity);
    }

    @Override
    public Collection<D> findAll() {
        return getResources().values();
    }

    @Override
    public void delete(ID id) {
        getResources().remove(id);
    }
}
