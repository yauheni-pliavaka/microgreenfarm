package com.example.microgreenfarm.services.map;

import com.example.microgreenfarm.dto.ShelvingUnitDto;
import com.example.microgreenfarm.enums.MicrogreenType;
import com.example.microgreenfarm.entity.ShelvingUnit;
import com.example.microgreenfarm.services.ShelvingUnitService;
import com.example.microgreenfarm.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class ShelvingUnitMapService extends AbstractMapService<ShelvingUnitDto, Long> implements ShelvingUnitService {

    private static final Map<Long, ShelvingUnitDto> resource = new HashMap<>();

    @Override
    public Map<Long, ShelvingUnitDto> getResources() {
        return resource;
    }

    @Override
    public Collection<ShelvingUnitDto> findByMicrogreenType(MicrogreenType microgreenType) {
        return null;
    }
}
