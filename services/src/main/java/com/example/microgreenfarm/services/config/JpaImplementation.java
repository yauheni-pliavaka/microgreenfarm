package com.example.microgreenfarm.services.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
//RetentionPolicy.SOURCE аннотация доступна в исходном коде и сбрасывается во время создания .class файла
//RetentionPolicy.CLASS аннотация хранится в .class файле, но недоступна при выполнении
//RetentionPolicy.SOURCE аннотация хранится в .class файле и доступна при выполнении
@Service
@ConditionalOnProperty(name = "jpa.enable", havingValue = "true")
//срабатывание по условию
public @interface JpaImplementation {
    //специфическая аннотация для сервисов
}

