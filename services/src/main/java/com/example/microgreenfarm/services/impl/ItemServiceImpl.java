package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.dto.ItemDto;
import com.example.microgreenfarm.entity.Item;
import com.example.microgreenfarm.entity.ItemPK;
import com.example.microgreenfarm.mapper.ItemMapper;
import com.example.microgreenfarm.repository.ItemRepository;
import com.example.microgreenfarm.services.ItemService;
import com.example.microgreenfarm.services.jpa.AbstractJpaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@RequiredArgsConstructor
@Service
public class ItemServiceImpl extends AbstractJpaService<ItemDto, Item, ItemPK> implements ItemService {

    private final ItemRepository itemRepository;
    private final ItemMapper mapper;

    @Override
    public ItemDto findById(ItemPK itemPK) {
        return super.findById(itemPK);
    }

    @Override
    public void save(ItemDto entity) {
        itemRepository.save(mapToEntity(entity));
    }

    @Override
    public Collection<ItemDto> findAll() {
        return super.findAll();
    }

    @Override
    public void delete(ItemPK itemPK) {
        itemRepository.deleteById(itemPK);
    }

    public ItemDto mapToDto(Item entity) {
        return mapper.mapToDto(entity);
    }

    public Item mapToEntity(ItemDto dto) {
        return mapper.map(dto);
    }

    @Override
    public JpaRepository<Item, ItemPK> getRepository() {
        return itemRepository;
    }
}
