package com.example.microgreenfarm.services.map;


import com.example.microgreenfarm.dto.MicrogreenDto;
import com.example.microgreenfarm.entity.Microgreen;
import com.example.microgreenfarm.enums.MicrogreenType;
import com.example.microgreenfarm.entity.ShelvingUnit;
import com.example.microgreenfarm.services.MicrogreenService;
import com.example.microgreenfarm.services.config.MapImplementation;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class MicrogreenMapService extends AbstractMapService<MicrogreenDto, Long> implements MicrogreenService {

    private static final Map<Long, MicrogreenDto> resource = new HashMap<>();

    @Override
    public Map<Long, MicrogreenDto> getResources() {
        return resource;
    }

    @Override
    public Microgreen findByShelvingUnit(ShelvingUnit shelvingUnit) {
        return null;
    }

    @Override
    public void updateCostPrice(Long microgreenId, BigDecimal costPrice) {
    }

    @Override
    public Microgreen findByHarvestDate(MicrogreenType microgreenType, OffsetDateTime harvestDate) {
        return null;
    }
}