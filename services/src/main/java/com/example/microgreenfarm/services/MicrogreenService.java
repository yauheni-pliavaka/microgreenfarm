package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.MicrogreenDto;
import com.example.microgreenfarm.entity.Microgreen;
import com.example.microgreenfarm.enums.MicrogreenType;
import com.example.microgreenfarm.entity.ShelvingUnit;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

public interface MicrogreenService extends CrudService<MicrogreenDto, Long> {

    Microgreen findByShelvingUnit(ShelvingUnit shelvingUnit);

    void updateCostPrice(Long microgreenId, BigDecimal costPrice);

    Microgreen findByHarvestDate(MicrogreenType microgreenType, OffsetDateTime harvestDate);
}
