package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.ProviderDto;

public interface ProviderService extends CrudService<ProviderDto, Long> {
}
