package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.SystemLogDto;

import java.util.List;

public interface SystemLogService {

    List<SystemLogDto> getAll();

    void createLogs(String activity, String message);
}
