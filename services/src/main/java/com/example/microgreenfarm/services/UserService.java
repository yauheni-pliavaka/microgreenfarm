package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.UserDto;

public interface UserService extends CrudService<UserDto, Long> {
}
