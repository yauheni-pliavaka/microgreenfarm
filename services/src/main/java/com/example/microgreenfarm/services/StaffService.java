package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.StaffDto;

public interface StaffService extends CrudService<StaffDto, Long> {
}
