package com.example.microgreenfarm.services;

import com.example.microgreenfarm.dto.ChildDto;

public interface ChildService extends CrudService<ChildDto, Long> {
}
