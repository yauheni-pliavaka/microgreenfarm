package com.example.microgreenfarm.services.jpa;

import com.example.microgreenfarm.entity.BaseEntity;
import com.example.microgreenfarm.services.BaseJpaService;
import com.example.microgreenfarm.services.CrudService;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public abstract class AbstractJpaService<D, T extends BaseEntity<ID>, ID> implements CrudService<D, ID>, BaseJpaService<T, ID> {

    public T findEntityById(ID id) {
        return getRepository().findById(id)
                .orElseThrow(() -> new NoSuchElementException("not found"));
        //если null, то кидаем exception, чтобы избежать NullPointerException
    }

    @Override
    public D findById(ID id) {
        return mapToDto(findEntityById(id));
    }

    @Override
    public void save(D dto) {
        getRepository().save(mapToEntity(dto));
    }

    @Override
    public Collection<D> findAll() {
        return getRepository().findAll().stream().map(this::mapToDto).collect(Collectors.toList());
        //не возвращает null, возвращает пустую коллекцию
    }

    @Override
    public void delete(ID id) {
        getRepository().deleteById(id);
    }

    public abstract D mapToDto(T entity);

    public abstract T mapToEntity(D dto);
}
