package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.dto.ChildDto;
import com.example.microgreenfarm.entity.Child;
import com.example.microgreenfarm.mapper.ChildMapper;
import com.example.microgreenfarm.repository.ChildRepository;
import com.example.microgreenfarm.services.ChildService;
import com.example.microgreenfarm.services.jpa.AbstractJpaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ChildServiceImpl extends AbstractJpaService<ChildDto, Child, Long> implements ChildService {

    private final ChildRepository childRepository;
    private final ChildMapper mapper;

    @Override
    public ChildDto mapToDto(Child entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Child mapToEntity(ChildDto dto) {
        return mapper.map(dto);
    }

    @Override
    public JpaRepository<Child, Long> getRepository() {
        return childRepository;
    }
}

