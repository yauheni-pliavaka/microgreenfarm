package com.example.microgreenfarm.services.impl;

import com.example.microgreenfarm.dto.NotesDto;
import com.example.microgreenfarm.entity.Notes;
import com.example.microgreenfarm.mapper.NotesMapper;
import com.example.microgreenfarm.repository.NotesRepository;
import com.example.microgreenfarm.services.NotesService;
import com.example.microgreenfarm.services.jpa.AbstractJpaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@RequiredArgsConstructor
@Service
public class NotesServiceImpl extends AbstractJpaService<NotesDto, Notes, Long> implements NotesService {

    private final NotesRepository notesRepository;
    private final NotesMapper mapper;

    @Override
    public NotesDto findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void save(NotesDto entity) {
        notesRepository.save(mapToEntity(entity));
    }

    @Override
    public Collection<NotesDto> findAll() {
        return super.findAll();
    }

    @Override
    public void delete(Long id) {
        notesRepository.deleteById(id);
    }

    public NotesDto mapToDto(Notes entity) {
        return mapper.mapToDto(entity);
    }

    public Notes mapToEntity(NotesDto dto) {
        return mapper.map(dto);
    }

    @Override
    public JpaRepository<Notes, Long> getRepository() {
        return notesRepository;
    }
}
