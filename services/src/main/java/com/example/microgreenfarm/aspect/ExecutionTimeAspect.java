package com.example.microgreenfarm.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.logging.Level;
import java.util.logging.Logger;

@Aspect
@Component
public class ExecutionTimeAspect {
    private Logger logger = Logger.getLogger(ExecutionTimeAspect.class.getName());

    @Around("@annotation(LogExecutionTime)")  //действие и до и после
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();

        Object proceed = joinPoint.proceed();

        long executionTime = System.currentTimeMillis() - start;

        logger.log(Level.INFO, joinPoint.getSignature() + " executed in " + executionTime + "ms");
        return proceed;
    }
}
