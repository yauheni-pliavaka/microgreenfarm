package com.example.microgreenfarm.aspect;

import com.example.listener.AuditLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Slf4j
@Aspect
@Component
public class ActivityLogAspect {

    @Autowired
    private AuditLogService auditLogService;

    @After("@annotation(activityLog)")  //когда должен сработать аспект
    public void logAction(JoinPoint joinPoint, ActivityLog activityLog) {
        String activity = activityLog.activity();
        String msg = activityLog.value();

        String[] names = StringUtils.substringsBetween(msg, "{", "}");  //чтобы отобразить ID в логах
        if (!ArrayUtils.isEmpty(names)) {
            msg = prepareMsg(msg, names, joinPoint);
        }

        log.debug("Logging activity: {} msg: {}", activity, msg);
        auditLogService.createLogs(activity, msg);
    }

    private String prepareMsg(String msg, String[] names, JoinPoint joinPoint) {
        int size = names.length;
        String[] values = new String[size];

        for (int i = 0; i < names.length; i++) {
            String name = names[i];
            Object value = getParameter(joinPoint, name);

            String valueStr = Objects.isNull(value) ? "UNDEFINED" : String.valueOf(value);
            values[i] = valueStr;
            names[i] = "{" + name + "}";
        }

        return StringUtils.replaceEach(msg, names, values);
    }

    private Object getParameter(JoinPoint joinPoint, String name) {
        if (Objects.nonNull(joinPoint) && joinPoint.getSignature() instanceof MethodSignature
                && Objects.nonNull(name)) {
            MethodSignature method = (MethodSignature) joinPoint.getSignature();
            String[] params = method.getParameterNames();

            for (int i = 0; i < params.length; i++) {
                if (Objects.nonNull(params[i]) && params[i].equals(name)) {
                    Object[] objects = joinPoint.getArgs();
                    return objects[i];
                }
            }
        }
        return null;
    }
}
