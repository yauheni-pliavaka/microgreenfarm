package com.example.microgreenfarm.dto;

import lombok.Data;

import java.time.OffsetDateTime;
import java.util.List;

@Data
public class ShelvingUnitDto {

    private Long id;
    private OffsetDateTime lightTime;
    private OffsetDateTime wateringTime;
    private int capacity;
    private List<MicrogreenDto> microgreens;
}
