package com.example.microgreenfarm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.OffsetDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)  //чтобы не слать null на UI
@JsonIgnoreProperties(ignoreUnknown = true)  //если придет неописанная проперти, она не будет учитываться
@Data
public class SystemLogDto {

    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    //для отображения данных на UI в виде yyyy-MM-dd HH:mm:ss
    private OffsetDateTime createdAt;

    private String activity;

    private String message;
}
