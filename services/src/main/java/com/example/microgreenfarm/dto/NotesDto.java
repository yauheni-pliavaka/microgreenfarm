package com.example.microgreenfarm.dto;

import lombok.Data;

@Data
public class NotesDto {

    private Long id;
    private String description;
    private OrderDto order;
}
