package com.example.microgreenfarm.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ItemDto {

    private String code;
    private String codePart;
    private String name;
    private BigDecimal price;
    private String description;
    private ReceiptDto receipt;
}
