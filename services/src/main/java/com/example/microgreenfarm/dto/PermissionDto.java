package com.example.microgreenfarm.dto;

import lombok.Data;

import java.util.List;

@Data
public class PermissionDto {

    private Long id;
    private String name;
    private List<UserDto> users;
}
