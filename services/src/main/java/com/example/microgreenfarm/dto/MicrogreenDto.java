package com.example.microgreenfarm.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@Data
public class MicrogreenDto {

    private Long id;
    private OffsetDateTime landingDate;
    private OffsetDateTime harvestDate;
    private BigDecimal costPrice;
}
