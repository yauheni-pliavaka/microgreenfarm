package com.example.microgreenfarm.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OrderDto {

    private Long id;
    private OffsetDateTime time;
    private ClientDto client;
    private ReceiptDto receipt;
    private NotesDto notes;
}
