package com.example.microgreenfarm.dto;

import lombok.Data;

import java.util.Set;

@Data
public class UserDto {

    private Long id;
    private String login;
    private String password;
    private Set<PermissionDto> permissions;
}
