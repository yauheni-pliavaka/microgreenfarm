package com.example.microgreenfarm.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;

@Data
public class ClientDto {

    private Long id;
    private List<ChildDto> children;
    private List<OrderDto> orders;
    private ContactDetailsDto contactDetails;
    private String firstName;
    private String lastName;
    private String fullName;

    private LocalDate birthDate;
    private OffsetDateTime createdAt;
    private String createdBy;
    private OffsetDateTime lastModifiedAt;
    private String lastModifiedBy;
}
