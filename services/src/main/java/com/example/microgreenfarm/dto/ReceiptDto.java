package com.example.microgreenfarm.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ReceiptDto {

    private Long id;
    private BigDecimal amount;
    private BigDecimal amountInUsd;
    private List<ItemDto> items;
    private OrderDto order;
}
