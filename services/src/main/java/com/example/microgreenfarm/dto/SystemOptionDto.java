package com.example.microgreenfarm.dto;

import lombok.Data;

@Data
public class SystemOptionDto {

    private String id;
    private String value;
}
