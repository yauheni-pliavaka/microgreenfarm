package com.example.microgreenfarm.dto;

import lombok.Data;

@Data
public class CourierDto {

    private String courierDescription;
}
