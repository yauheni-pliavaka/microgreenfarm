package com.example.microgreenfarm.dto;

import lombok.Data;

@Data
public class ContactDetailsDto {

    private String phone;
    private String city;
    private String street;
    private String building;
}
