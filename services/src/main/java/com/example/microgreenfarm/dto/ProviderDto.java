package com.example.microgreenfarm.dto;

import lombok.Data;

@Data
public class ProviderDto {

    private String providerDescription;
}
