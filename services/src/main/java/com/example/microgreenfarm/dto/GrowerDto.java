package com.example.microgreenfarm.dto;

import lombok.Data;

@Data
public class GrowerDto {

    private String growerDescription;
}
