INSERT INTO permission (name) values ('update_user');
INSERT INTO permission (name) values ('delete_user');
INSERT INTO permission (name) values ('create_order');
INSERT INTO permission (name) values ('get_children');

INSERT INTO uom (code, description) values ('CONTAINER', 'container 11x18');
INSERT INTO uom (code, description) values ('CUT_GREENS', 'cut greens 100g');
INSERT INTO uom (code, description) values ('GIFT_SET', 'gift box 12x20');