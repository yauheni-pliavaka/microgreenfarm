package com.example.microgreenfarm.controller;

import com.example.microgreenfarm.services.MicrogreenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@RequiredArgsConstructor
@Controller
public class MicrogreenController {

    private final MicrogreenService microgreenService;
}
