package com.example.microgreenfarm.controller;

import com.example.microgreenfarm.services.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@RequiredArgsConstructor
@Controller
public class ClientController {

    private final ClientService clientService;
}
