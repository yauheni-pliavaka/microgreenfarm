package com.example.microgreenfarm.controller;

import com.example.microgreenfarm.services.ShelvingUnitService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@RequiredArgsConstructor
@Controller
public class ShelvingUnitController {

    private final ShelvingUnitService shelvingUnitService;
}
