package com.example.microgreenfarm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.example.listener", "com.example.microgreenfarm"})
@SpringBootApplication
public class MicrogreenFarmApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicrogreenFarmApplication.class, args);
    }

}
