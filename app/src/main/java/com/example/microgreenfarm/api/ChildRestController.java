package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.ChildDto;
import com.example.microgreenfarm.services.ChildService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/children")
@RestController
public class ChildRestController {

    private final ChildService childService;

    @GetMapping()
    public Collection<ChildDto> getAll() {
        return childService.findAll();
    }
}
