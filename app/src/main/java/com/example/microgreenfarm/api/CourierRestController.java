package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.CourierDto;
import com.example.microgreenfarm.services.CourierService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/couriers")
@RestController
public class CourierRestController {

    private final CourierService courierService;

    @GetMapping()
    public Collection<CourierDto> getAll() {
        return courierService.findAll();
    }
}
