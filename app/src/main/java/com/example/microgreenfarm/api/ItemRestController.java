package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.ItemDto;
import com.example.microgreenfarm.services.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/items")
@RestController
public class ItemRestController {

    private final ItemService itemService;

    @GetMapping()
    public Collection<ItemDto> getAll() {
        return itemService.findAll();
    }
}
