package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.ClientDto;
import com.example.microgreenfarm.entity.Client;
import com.example.microgreenfarm.services.ClientService;
import com.example.microgreenfarm.specification.filter.ClientFilter;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/api/clients")
@RestController
public class ClientRestController {

    private final ClientService clientService;

    @GetMapping()
    public Collection<ClientDto> getAll() {
        return clientService.findAll();
    }

    @ApiOperation(value = "Search clients", response = Client.class, responseContainer = "Page")
    @GetMapping("/search")
    public Page<ClientDto> search(@RequestParam(required = false) String term,
                                  @RequestParam(required = false) Integer pageNumber) {

        ClientFilter clientFilter = ClientFilter.builder()
                .pageNumber(pageNumber)
                .term(term)
                .build();
        return clientService.search(clientFilter);
    }

    @ApiOperation(value = "Search clients", response = Client.class, responseContainer = "List")
    @GetMapping("/search/by_name")
    public List<Client> searchByName(@RequestParam(required = false) String firstName) {

        ClientFilter clientFilter = ClientFilter.builder()
                .firstName(firstName)
                .build();
        return clientService.searchByName(firstName);
    }

    @ApiOperation(value = "Find clients", response = Client.class, responseContainer = "List")
    @PutMapping("/find")  //не можем сделать @GetMapping из-за @RequestBody
    public List<ClientDto> findClient(@ApiParam(value = "Search example") @RequestBody ClientDto clientDto) {
        return clientService.findClient(clientDto);
    }

    @GetMapping("/{id}")
    public ClientDto getById(@PathVariable("id") Long clientId) {
        return clientService.findById(clientId);
    }

    @PostMapping
    public void create(@RequestBody ClientDto clientDto) {
        clientService.save(clientDto);
    }

    @PutMapping("/{id}")
    public void updateFullName(@PathVariable("id") Long clientId, String newFullName) {
        clientService.updateFullName(clientId, newFullName);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long clientId) {
        clientService.delete(clientId);
    }
}
