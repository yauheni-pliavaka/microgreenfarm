package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.ReceiptDto;
import com.example.microgreenfarm.services.ReceiptService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/receipts")
@RestController
public class ReceiptRestController {

    private final ReceiptService receiptService;

    @GetMapping()
    public Collection<ReceiptDto> getAll() {
        return receiptService.findAll();
    }
}
