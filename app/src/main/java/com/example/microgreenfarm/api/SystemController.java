package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.SystemLogDto;
import com.example.microgreenfarm.dto.SystemOptionDto;
import com.example.microgreenfarm.services.SystemLogService;
import com.example.microgreenfarm.services.SystemOptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/api/system")
@RestController
public class SystemController {

    private final SystemLogService systemLogService;
    private final SystemOptionService systemOptionService;

    @GetMapping("/logs")
    public List<SystemLogDto> getLogs() {
        return systemLogService.getAll();
    }

    @GetMapping("/options")
    public List<SystemOptionDto> getSystem() {
        return systemOptionService.getAll();
    }

    @GetMapping("/options/{id}")
    public SystemOptionDto getById(@PathVariable("id") String optionKey) {
        return systemOptionService.getById(optionKey);
    }

    @PutMapping("/options")
    public void saveOption(@RequestBody SystemOptionDto option) {
        systemOptionService.save(option);
    }
}
