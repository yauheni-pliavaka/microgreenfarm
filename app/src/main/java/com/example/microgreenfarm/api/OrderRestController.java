package com.example.microgreenfarm.api;

import com.example.microgreenfarm.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RequestMapping("/api/orders")
@RestController
public class OrderRestController {

    private final OrderService orderService;

    @PutMapping("/{id}/cancel")
    public void cancelOrder(@PathVariable Long id) {
        orderService.cancelOrder(id);
    }

    @PutMapping("/{id}/payed")
    public void markOrderAsPayed(@PathVariable Long id) {
        orderService.markOrderAsPayed(id);
    }
}
