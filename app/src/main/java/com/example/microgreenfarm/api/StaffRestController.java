package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.StaffDto;
import com.example.microgreenfarm.services.StaffService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/staff")
@RestController
public class StaffRestController {

    private final StaffService staffService;

    @GetMapping()
    public Collection<StaffDto> getAll() {
        return staffService.findAll();
    }
}
