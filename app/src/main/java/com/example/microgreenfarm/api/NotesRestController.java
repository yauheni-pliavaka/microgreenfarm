package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.NotesDto;
import com.example.microgreenfarm.services.NotesService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/notes")
@RestController
public class NotesRestController {

    private final NotesService notesService;

    @GetMapping()
    public Collection<NotesDto> getAll() {
        return notesService.findAll();
    }
}
