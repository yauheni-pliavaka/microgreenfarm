package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.GrowerDto;
import com.example.microgreenfarm.services.GrowerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/growers")
@RestController
public class GrowerRestController {

    private final GrowerService growerService;

    @GetMapping()
    public Collection<GrowerDto> getAll() {
        return growerService.findAll();
    }
}
