package com.example.microgreenfarm.api;

import com.example.microgreenfarm.entity.ClientReceipt;
import com.example.microgreenfarm.repository.ClientReceiptRepository;
import com.example.microgreenfarm.repository.ClientRepository;
import com.example.microgreenfarm.services.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/api")
@RestController
public class TestController {

    private final ClientReceiptRepository clientReceiptRepository;

    @GetMapping("/view")
    public List<ClientReceipt> getView() {
        return clientReceiptRepository.findByPayerNameContainsIgnoreCase("a");
    }
}
