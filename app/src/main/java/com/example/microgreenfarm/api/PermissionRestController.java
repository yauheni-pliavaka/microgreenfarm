package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.PermissionDto;
import com.example.microgreenfarm.services.PermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/permissions")
@RestController
public class PermissionRestController {

    private final PermissionService permissionService;

    @GetMapping()
    public Collection<PermissionDto> getAll() {
        return permissionService.findAll();
    }
}
