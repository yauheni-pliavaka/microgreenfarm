package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.ShelvingUnitDto;
import com.example.microgreenfarm.services.ShelvingUnitService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/shelvingUnits")
@RestController
public class ShelvingUnitRestController {

    private final ShelvingUnitService shelvingUnitService;

    @GetMapping()
    public Collection<ShelvingUnitDto> getAll() {
        return shelvingUnitService.findAll();
    }
}
