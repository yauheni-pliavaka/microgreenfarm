package com.example.microgreenfarm.api;


import com.example.microgreenfarm.dto.ProviderDto;
import com.example.microgreenfarm.services.ProviderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/providers")
@RestController
public class ProviderRestController {

    private final ProviderService providerService;

    @GetMapping()
    public Collection<ProviderDto> getAll() {
        return providerService.findAll();
    }
}
