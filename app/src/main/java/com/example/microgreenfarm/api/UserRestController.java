package com.example.microgreenfarm.api;

import com.example.microgreenfarm.dto.UserDto;
import com.example.microgreenfarm.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/users")
@RestController
public class UserRestController {

    private final UserService userService;

    @GetMapping()
    public Collection<UserDto> getAll() {
        return userService.findAll();
    }
}
