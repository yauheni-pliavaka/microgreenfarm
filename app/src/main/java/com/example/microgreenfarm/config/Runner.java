package com.example.microgreenfarm.config;

import com.example.microgreenfarm.entity.*;
import com.example.microgreenfarm.enums.MicrogreenType;
import com.example.microgreenfarm.enums.Role;
import com.example.microgreenfarm.enums.Sex;
import com.example.microgreenfarm.enums.UOM;
import com.example.microgreenfarm.repository.*;
import lombok.RequiredArgsConstructor;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Component("mainRunner")
public class Runner implements CommandLineRunner {

    @Value("${spring.datasource.password:undefined}")
    private String dbPwd;

    @Qualifier("customEncryptor")
    @Autowired
    StringEncryptor stringEncryptor;

    private final ItemRepository itemRepository;
    private final OrderRepository orderRepository;
    private final NotesRepository notesRepository;
    private final UOMRepository uomRepository;
    private final ClientRepository clientRepository;
    private final ReceiptRepository receiptRepository;
    private final PermissionRepository permissionRepository;
    private final UserRepository userRepository;
    private final MicrogreenRepository microgreenRepository;
    private final StaffRepository staffRepository;

    @Override
    public void run(String... args) throws Exception {
//        initData();

//        createUser();

//        encryption();

//        createClient();

//        createOrder();

//        createReceiptWithItems();

//        createMicrogreen();

//        createStaff();
    }

    private void createStaff() {
        Courier courier = Courier.builder()
                .firstName("Ivan")
                .lastName("Ivanov")
                .courierDescription("courier")
                .build();

        Grower grower = Grower.builder()
                .firstName("Ivan")
                .lastName("Ivanov")
                .growerDescription("grower")
                .build();

        Provider provider = Provider.builder()
                .firstName("Ivan")
                .lastName("Ivanov")
                .providerDescription("provider")
                .build();

        staffRepository.saveAll(Arrays.asList(courier, grower, provider));
    }

    private void createMicrogreen() {
        Microgreen pea = Microgreen.builder()
                .costPrice(BigDecimal.ONE)
                .landingDate(OffsetDateTime.now())
                .harvestDate(OffsetDateTime.now())
                .microgreenType(MicrogreenType.PEA)
                .build();

        Microgreen radish = Microgreen.builder()
                .costPrice(BigDecimal.ONE)
                .landingDate(OffsetDateTime.now())
                .harvestDate(OffsetDateTime.now())
                .microgreenType(MicrogreenType.RADISH)
                .build();

        Microgreen sunflower = Microgreen.builder()
                .costPrice(BigDecimal.ONE)
                .landingDate(OffsetDateTime.now())
                .harvestDate(OffsetDateTime.now())
                .microgreenType(MicrogreenType.SUNFLOWER)
                .build();

        microgreenRepository.saveAll(Arrays.asList(pea, radish, sunflower));
    }

    private void createUser() {
//        Permission permDef = new Permission();

        Permission perm = permissionRepository.findByName("update_user")
                .orElseThrow(NoSuchElementException::new);

//        Permission perm2 = permissionRepository.findByName("update_user")
//                .orElse(permDef);

        User user1 = User.builder()
                .login("user1")
                .password("1234")
                .role(Role.SELLER)
                .permissions(Collections.singleton(perm))
                .build();

        User user2 = User.builder()
                .login("user2")
                .password("4321")
                .role(Role.CLIENT)
                .permissions(Collections.singleton(perm))
                .build();

//        String pwd = userRepository.findById(1L).map(User::getPassword).orElse("undefined");
        userRepository.saveAll(Arrays.asList(user1, user2));
    }

    private void initData() {
        Permission permission1 = Permission.builder()
                .name("update_user")
                .build();

        Permission permission2 = Permission.builder()
                .name("delete_user")
                .build();

        Permission permission3 = Permission.builder()
                .name("create_order")
                .build();

        Permission permission4 = Permission.builder()
                .name("get_children")
                .build();

        permissionRepository.saveAll(Arrays.asList(permission1, permission2, permission3, permission4));

        UnitOfMeasure unitOfMeasure1 = UnitOfMeasure.builder()
                .code(UOM.CONTAINER)
                .description("container 11x18")
                .build();

        UnitOfMeasure unitOfMeasure2 = UnitOfMeasure.builder()
                .code(UOM.CUT_GREENS)
                .description("cut greens 100g")
                .build();

        UnitOfMeasure unitOfMeasure3 = UnitOfMeasure.builder()
                .code(UOM.GIFT_SET)
                .description("gift box 12x20")
                .build();

        uomRepository.saveAll(Arrays.asList(unitOfMeasure1, unitOfMeasure2, unitOfMeasure3));
    }

    private void createClient() {
        Client client = Client.builder()
                .firstName("Yauheni")
                .lastName("Pliavaka")
                .discount(10L)
                .sex(Sex.MALE)
                .contactDetails(ContactDetails.builder()
                        .phone("375295080737")
                        .city("Minsk")
                        .street("Street")
                        .building("1")
                        .build())
                .build();

        clientRepository.save(client);
    }

    private void createReceiptWithItems() {
        List<Order> orders = orderRepository.findAll();

        UnitOfMeasure uom = uomRepository.findById(UOM.CONTAINER).orElseThrow();

        Receipt receipt = Receipt.builder()
                .amount(BigDecimal.TEN)
                .order(orders.get(0))
                .build();

        Item item1 = Item.builder()
                .name("Pea")
                .price(BigDecimal.ONE)
                .receipt(receipt)   //должны устанавливать receipt здесь, потому что Item - owning side
                .id(ItemPK.builder()
                        .code("001")
                        .codePart("abc")
                        .build())
                .unitOfMeasure(uom)
                .build();

        Item item2 = Item.builder()
                .name("Radish")
                .price(BigDecimal.ONE)
                .id(ItemPK.builder()
                        .code("002")
                        .codePart("abc")
                        .build())
                .unitOfMeasure(uom)
                .build();

//        itemRepository.saveAll(Arrays.asList(item1, item2));
        receipt.setItems(Arrays.asList(item1, item2));

        receiptRepository.save(receipt);

        System.out.println("Amount in usd = " + receipt.getAmountInUsd());
    }

    private void createOrder() {
//        List<Client> clients = clientRepository.findAll();

        Notes notes1 = Notes.builder()
                .description("desc1")
                .build();
        Notes notes2 = Notes.builder()
                .description("desc2")
                .build();

        Order order1 = Order.builder()
                .time(OffsetDateTime.now())
                .notes(notes1)
//                .client(clients.get(0))
                .build();
        Order order2 = Order.builder()
                .time(OffsetDateTime.now())
                .notes(notes2)
//                .client(clients.get(0))
                .build();

//        for (Client client : clients) {
//            client.setOrder(orderRepository.findAll());
//        }

        orderRepository.saveAll(Arrays.asList(order1, order2));
//        clientRepository.saveAll(clients);
    }

    private void encryption() {
//        String pwd = "prod_pwd";
//        String encrypt = stringEncryptor.encrypt(pwd);
//        System.out.println(encrypt);
//
//        String decrypt = stringEncryptor.decrypt(encrypt);
//        System.out.println(decrypt);

        System.out.println(dbPwd);
    }
}

