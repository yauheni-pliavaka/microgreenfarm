package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Grower;

public interface GrowerRepository extends PersonRepository<Grower, Long> {
}
