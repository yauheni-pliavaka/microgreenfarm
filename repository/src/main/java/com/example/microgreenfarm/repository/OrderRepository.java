package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "SELECT o FROM Order o where o.client.fullName like :clientFullName")
    Optional<Order> findByFullName(@Param("clientFullName") String fullName);

    //удалить отмененные заказы
    @Transactional  //чтобы откатилось, если что-то поломается
    @Modifying  //обязательно
    @Query("DELETE from Order o where o.status = com.example.microgreenfarm.enums.OrderStatus.CANCELLED")
        //указываем полное обращение к элементу енама, потому что маппинг может поменяться
    void deleteCancelled();
    //непосредственно в БД без участия персистенс контекста удаляет данные
}
