package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Provider;

public interface ProviderRepository extends PersonRepository<Provider, Long> {
}
