package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Microgreen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MicrogreenRepository extends JpaRepository<Microgreen, Long> {
}
