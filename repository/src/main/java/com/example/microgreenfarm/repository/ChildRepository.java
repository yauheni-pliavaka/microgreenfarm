package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Child;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChildRepository extends JpaRepository<Child, Long> {
}
