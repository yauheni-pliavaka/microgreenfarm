package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Notes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotesRepository extends JpaRepository<Notes, Long> {
}
