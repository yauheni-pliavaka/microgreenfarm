package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.ClientReceipt;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientReceiptRepository extends JpaRepository<ClientReceipt, Long> {

    List<ClientReceipt> findByPayerNameContainsIgnoreCase(String name);
}
