package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface PersonRepository<T extends Person, ID> extends JpaRepository<T, ID> {
    //будет проверять относительно конечных репозиториев

    List<T> findByFirstName(String firstName);
    List<T> findByLastName(String lastName);
    List<T> findByFullName(String fullName);
}
