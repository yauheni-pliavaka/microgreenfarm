package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Staff;

public interface StaffRepository extends PersonRepository<Staff, Long> {
}
