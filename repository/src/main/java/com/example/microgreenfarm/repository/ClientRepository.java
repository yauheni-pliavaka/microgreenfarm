package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Client;
import com.example.microgreenfarm.specification.SearchableRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.LocalDate;
import java.util.List;

public interface ClientRepository extends PersonRepository<Client, Long>, SearchableRepository<Client, Long> {
    //JpaSpecificationExecutor для того, чтобы репозиторий работал со спецификацией

    List<Client> findByBirthDate(LocalDate birthDate);

    List<Client> findByFullNameAndChildrenIsNull(String fullName); //если есть дети - подарок

    List<Client> findByLastNameOrFirstName(String lastName, String firstName);
}
