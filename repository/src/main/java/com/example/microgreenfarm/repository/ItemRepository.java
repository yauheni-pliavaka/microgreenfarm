package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Item;
import com.example.microgreenfarm.entity.ItemPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, ItemPK> {
}
