package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.entity.Courier;

public interface CourierRepository extends PersonRepository<Courier, Long> {
}
