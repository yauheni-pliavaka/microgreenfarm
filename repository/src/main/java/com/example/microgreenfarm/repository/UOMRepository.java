package com.example.microgreenfarm.repository;

import com.example.microgreenfarm.enums.UOM;
import com.example.microgreenfarm.entity.UnitOfMeasure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UOMRepository extends JpaRepository<UnitOfMeasure, UOM> {
}
