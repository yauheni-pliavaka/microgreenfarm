package com.example.microgreenfarm.specification;

import com.example.microgreenfarm.entity.Client;
import com.example.microgreenfarm.entity.Client_;
import com.example.microgreenfarm.entity.ContactDetails_;
import com.example.microgreenfarm.specification.filter.ClientFilter;
import com.example.microgreenfarm.specification.filter.SpecBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public interface ClientSpecification extends Specification<Client> {

    static ClientSpecification hasFirstName(String value) {
        return (root, cq, cb) -> cb.equal(cb.lower(root.get(Client_.FIRST_NAME)), value.toLowerCase());
    }

    static ClientSpecification firstNameLike(String value) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get(Client_.FIRST_NAME)), "%" + value.toLowerCase() + "%");
        //Root, CriteriaQuery, CriteriaBuilder преобразуем в Specification
    }

    static ClientSpecification hasLastName(String value) {
        return (root, cq, cb) -> cb.equal(cb.lower(root.get(Client_.LAST_NAME)), value.toLowerCase());
    }

    static ClientSpecification lastNameLike(String value) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get(Client_.LAST_NAME)), "%" + value.toLowerCase() + "%");
    }

    static ClientSpecification cityLike(String value) {
        return (root, cq, cb) -> cb.like(cb.lower(root.join(Client_.CONTACT_DETAILS).get(ContactDetails_.CITY)),
                "%" + value.toLowerCase() + "%");
    }

    static ClientSpecification streetLike(String value) {
        return (root, cq, cb) -> cb.like(cb.lower(root.join(Client_.CONTACT_DETAILS).get(ContactDetails_.STREET)),
                "%" + value.toLowerCase() + "%");
    }

    static Specification<Client> findByTerm(String term) {
        List<Specification<Client>> specList = new ArrayList<>();
        specList.add(firstNameLike(term));
        specList.add(lastNameLike(term));
        specList.add(cityLike(term));
        specList.add(streetLike(term));

        return SpecificationComposer.compose(specList, Predicate.BooleanOperator.OR);
    }

    static ClientSpecification.Builder builder() {
        return new ClientSpecification.Builder();
    }

    class Builder extends SpecBuilder<Client, ClientFilter> {

        @Override
        public Specification<Client> build() {
            List<Specification<Client>> specList = new ArrayList<>();

            if (StringUtils.isNotEmpty(filter.getFirstName())) {
                specList.add(hasFirstName(filter.getFirstName()));
            }

            if (StringUtils.isNotEmpty(filter.getLastName())) {
                specList.add(hasLastName(filter.getLastName()));
            }

            if (StringUtils.isNotEmpty(filter.getTerm())) {
                specList.add(findByTerm(filter.getTerm()));
            }

            return SpecificationComposer.compose(specList);
        }
    }
}
