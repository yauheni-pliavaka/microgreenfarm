package com.example.microgreenfarm.specification.filter;

import com.example.microgreenfarm.entity.Client;
import com.example.microgreenfarm.entity.Client_;
import com.example.microgreenfarm.specification.ClientSpecification;
import lombok.Builder;
import lombok.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

@Builder
@Value  //делает все поля private final (immutable class)
public class ClientFilter implements Filter<Client> {

    private static final Sort DEFAULT_SORTING = Sort.by(Sort.Direction.DESC, Client_.CREATED_AT);
    //сортировка по дате создания

    Integer pageNumber;
    String term;

    String firstName;
    String lastName;
    boolean isActive;

    @Override
    public Pageable getPageable() {
        int page = Objects.isNull(pageNumber) ? 0 : pageNumber - 1;
        return PageRequest.of(page, DEFAULT_PAGE_SIZE, DEFAULT_SORTING);
    }

    @Override
    public Specification<Client> getSpecification() {
        return ClientSpecification.builder().filter(this).build();
    }
}
