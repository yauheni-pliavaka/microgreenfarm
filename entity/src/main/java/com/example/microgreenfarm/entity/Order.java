package com.example.microgreenfarm.entity;

import com.example.microgreenfarm.enums.OrderStatus;
import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "CLIENT_ORDER")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "TIME")
    private OffsetDateTime time;

    @Builder.Default  //так как используем билдер в раннере
    @Enumerated(EnumType.STRING)
    private OrderStatus status = OrderStatus.PENDING;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "CLIENT_ID")
    private Client client;

    @OneToOne
    @JoinColumn(name = "RECEIPT_ID")
    private Receipt receipt;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    //orphanRemoval - энтити гарантированно удаляется, если ни с чем не связана
    @JoinColumn(name = "ORDER_NOTE_ID")
    private Notes notes;
}
