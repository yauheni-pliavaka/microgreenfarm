package com.example.microgreenfarm.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import java.io.Serializable;


@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class ItemPK implements Serializable {
//можно написать custom generator

    @Column(name = "CODE")
    private String code;

    @Column(name = "CODE_PART")
    private String codePart;
}
