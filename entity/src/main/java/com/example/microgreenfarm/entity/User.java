package com.example.microgreenfarm.entity;

import com.example.microgreenfarm.converter.RoleConverter;
import com.example.microgreenfarm.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "USER")
public class User extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "PASSWORD")
    private String password;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH})
    @JoinTable(name = "USER_PERM",
            joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "PERM_ID")
    )
    private Set<Permission> permissions;

//    @Enumerated(EnumType.STRING)
    //может возникнуть проблема, если в таблицу загружаются данные с другими названиями
    @Convert(converter = RoleConverter.class)
    private Role role;
    //по дефолту передается порядковый номер енама, лучше не использовать
}
