package com.example.microgreenfarm.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "CLIENT")
public class Client extends Person{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DISCOUNT")
    private Long discount;

    @Embedded
    private ContactDetails contactDetails;

    @ManyToMany
    private List<Child> children;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
    private List<Order> order;
}
