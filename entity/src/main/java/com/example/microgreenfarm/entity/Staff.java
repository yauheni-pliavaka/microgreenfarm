package com.example.microgreenfarm.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Staff extends Person {  //если сделать класс абстрактным, то таблица Staff не создастся

    @Id
    @GeneratedValue  //не используем GenerationType.IDENTITY чтобы ID был уникален во всей БД, а не только в Staff
    //при объединении таблиц ID могут совпасть
    @Column(name = "ID")
    private Long id;

    @Column(name = "WORK_TIME")
    private String workTime;

    @OneToMany(mappedBy = "staff", cascade = CascadeType.ALL)
    private List<ShelvingUnit> shelvingUnit;
}
