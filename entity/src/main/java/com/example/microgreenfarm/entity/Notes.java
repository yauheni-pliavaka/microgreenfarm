package com.example.microgreenfarm.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "NOTES")
public class Notes extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DESCRIPTION")
    private String description;

    //необязательно делать двухстороннюю связь, если не планируем обращаться к order через notes
    @OneToOne(mappedBy = "notes")
    //настройки маппинга описаны над полем notes, колонка будет создана только в таблице Order
    //заметка без заказа не может существовать, а заказ без заметки может
    private Order order;
}
