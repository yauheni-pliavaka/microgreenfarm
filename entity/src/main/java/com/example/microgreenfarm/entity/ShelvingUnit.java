package com.example.microgreenfarm.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "SHELVING_UNIT")
public class ShelvingUnit extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "LIGHT_TIME")
    private OffsetDateTime lightTime;

    @Column(name = "WATERING_TIME")
    private OffsetDateTime wateringTime;

    @Column(name = "CAPACITY")
    private int capacity;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "STAFF_ID")
    private Staff staff;

    @ManyToMany
    private List<Microgreen> microgreen;
}
