package com.example.microgreenfarm.entity;

import com.example.microgreenfarm.annotation.View;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

//@Setter не ставим, потому что мы не можем обновлять представление данных в таблице
@Getter
@Entity
@View  //hibernate не ищет наследников @Table
@Table(name = "CLIENT_RECEIPT")
public class ClientReceipt {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "RECEIPT_ID")
    private Long receiptId;

    @Column(name = "TOTAL_AMOUNT")
    private BigDecimal totalAmount;

    @Column(name = "PAYER")
    private String payerName;

    @Column(name = "PAYER_PHONE")
    private String payerPhone;

}
