package com.example.microgreenfarm.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ITEM")
public class Item extends BaseEntity<ItemPK> {

    @EmbeddedId   //composite identifier
    private ItemPK id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PRICE")
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "UNIT_OF_MEASURE_CODE")
    private UnitOfMeasure unitOfMeasure;

    @ManyToOne(cascade = CascadeType.PERSIST)  //PERSIST - если итем обновляется/удаляется, то чек не должен удаляться
    @JoinColumn(name = "RECEIPT_ID")
    private Receipt receipt;
}
