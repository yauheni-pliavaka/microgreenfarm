package com.example.microgreenfarm.entity;

import lombok.*;
import org.apache.commons.collections4.CollectionUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "RECEIPT")
public class Receipt extends BaseEntity<Long> {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Transient  //чтобы поле не сохранялось в БД, так как курс постоянно меняется
    private BigDecimal amountInUsd;

    @OneToMany(mappedBy = "receipt", cascade = CascadeType.ALL)
    private List<Item> items;

    @OneToOne(mappedBy = "receipt")
    private Order order;

    @PrePersist
    @PreUpdate
    private void calculateAmount() {
        BigDecimal amount = BigDecimal.ZERO;
        if (CollectionUtils.isNotEmpty(items)) {  //проверяет на null и наличие итемов
            for (Item item : items) {
                amount = amount.add(item.getPrice());
            }
        }
        this.amount = amount;
    }

    @PostLoad  //после того как загрузили энтити в персистенс контекст
    @PostPersist  //когда сохранили энтити и возвращаем ее в сервис
    @PostUpdate  //когда обновили энтити
    private void calcAmountInUsd() {
        this.amountInUsd = amount.divide(BigDecimal.valueOf(2.5), 2, RoundingMode.HALF_UP);
        //divide(BigDecimal.valueOf(курс перевода), количество знаков после запятой, округление(в большую сторону после половины))
    }
}
