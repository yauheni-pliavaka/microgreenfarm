package com.example.microgreenfarm.entity;

import com.example.microgreenfarm.enums.UOM;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "UOM")
public class UnitOfMeasure {

    @Id
    @Enumerated(EnumType.STRING) //чтобы прописывалось в виде кода, а не порядкового номера
    @Column(name = "CODE")
    private UOM code;

    @Column(name = "DESCRIPTION")
    private String description;
}
