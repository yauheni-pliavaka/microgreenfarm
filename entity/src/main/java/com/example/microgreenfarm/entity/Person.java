package com.example.microgreenfarm.entity;

import com.example.microgreenfarm.enums.Sex;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@MappedSuperclass
public abstract class Person extends BaseEntity<Long> {

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "BIRTHDAY")
    private LocalDate birthDate;

    @Column(name = "SEX")
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @PrePersist
    @PreUpdate
    //перед тем как вставить запись в БД на создание/апдейт автоматически выполнить этот метод
    private void initFullName() {
        this.fullName = getFirstName() + " " + getLastName();
    }

}
