package com.example.microgreenfarm.entity;

import com.example.microgreenfarm.converter.MicrogreenTypeConverter;
import com.example.microgreenfarm.enums.MicrogreenType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "MICROGREEN")
public class Microgreen extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "LANDING_DATE")
    private OffsetDateTime landingDate;

    @Column(name = "HARVEST_DATE")
    private OffsetDateTime harvestDate;

    @Column(name = "COST_PRICE")
    private BigDecimal costPrice;

    @Column(name = "MICROGREEN_TYPE", nullable = false, updatable = false)
    @Convert(converter = MicrogreenTypeConverter.class)
    private MicrogreenType microgreenType;

    @ManyToMany(mappedBy = "microgreen")
    private List<ShelvingUnit> shelvingUnit;
}
