package com.example.microgreenfarm.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
public enum Role {

    ADMIN("adm"), SELLER("slr"), COURIER("cs"), CLIENT("user");

    private static final Map<String, Role> MAP = Arrays.stream(Role.values())
            .collect(Collectors.toMap(Role::getValue, Function.identity()));
    //Function.identity() берет значение, которое пришло в обработку
    //Collectors.toMap обработчик, возвращает HashMap

    private final String value;

    public static Role getByValue(String value) {
        if (Objects.isNull(value)) {
            throw new RuntimeException();
            //создать свой Exception
        }

        Role role = MAP.get(value);
        return Objects.isNull(role) ? CLIENT : role;   //если role=null или неизвестное значение, то по дефолту CLIENT
    }
}