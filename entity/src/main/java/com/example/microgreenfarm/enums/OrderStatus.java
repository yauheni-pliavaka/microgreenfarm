package com.example.microgreenfarm.enums;

public enum OrderStatus {
    PENDING, PAYED, CANCELLED
}
