package com.example.microgreenfarm.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
public enum MicrogreenType {

    PEA("pea"),
    RADISH("radish"),
    SUNFLOWER("sunflower");

    private final String value;

    private static final Map<String, MicrogreenType> MAP = Arrays.stream(MicrogreenType.values())
            .collect(Collectors.toMap(MicrogreenType::getValue, Function.identity()));
    //Function.identity() берет значение, которое пришло в обработку
    //Collectors.toMap обработчик, возвращает HashMap

    public static MicrogreenType getByValue(String value) {
        if (Objects.isNull(value)) {
            throw new RuntimeException();
            //создать свой Exception
        }
        return MAP.get(value);
    }
}
