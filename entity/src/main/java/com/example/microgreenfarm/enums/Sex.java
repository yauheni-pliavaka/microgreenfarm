package com.example.microgreenfarm.enums;

public enum Sex {
    MALE, FEMALE
}
