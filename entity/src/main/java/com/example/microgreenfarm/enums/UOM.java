package com.example.microgreenfarm.enums;

public enum UOM {
    CONTAINER, CUT_GREENS, GIFT_SET
}
