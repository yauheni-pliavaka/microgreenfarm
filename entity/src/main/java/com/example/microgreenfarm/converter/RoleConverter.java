package com.example.microgreenfarm.converter;

import com.example.microgreenfarm.enums.Role;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class RoleConverter implements AttributeConverter<Role, String> {

    @Override
    public String convertToDatabaseColumn(Role attribute) {
        return attribute.getValue();
    }

    @Override
    public Role convertToEntityAttribute(String dbData) {
        //можно сделать с помощью if else или switch, но каждый раз читая пользователя
        //с помощью Spring Security будет проверяться, имеет ли пользователь доступ к этой роли
        return Role.getByValue(dbData);
    }
}
