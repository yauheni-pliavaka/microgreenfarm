package com.example.microgreenfarm.converter;

import com.example.microgreenfarm.enums.MicrogreenType;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class MicrogreenTypeConverter implements AttributeConverter<MicrogreenType, String> {

    @Override
    public String convertToDatabaseColumn(MicrogreenType attribute) {
        return attribute.getValue();
    }

    @Override
    public MicrogreenType convertToEntityAttribute(String dbData) {
        //можно сделать с помощью if else или switch, но каждый раз читая пользователя
        //с помощью Spring Security будет проверяться, имеет ли пользователь доступ к этой роли
        return MicrogreenType.getByValue(dbData);
    }
}
