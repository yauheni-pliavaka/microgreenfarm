package com.example.microgreenfarm.annotation;

import org.hibernate.annotations.Immutable;

@Immutable  //позволяет скипать попытки проапдейтать
public @interface View {
}
