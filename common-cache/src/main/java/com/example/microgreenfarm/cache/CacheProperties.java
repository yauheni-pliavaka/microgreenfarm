package com.example.microgreenfarm.cache;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component   //разрешает использовать ConfigurationProperties и делает properties бином
@ConfigurationProperties(prefix = "cache.ttl")
public class CacheProperties {

//    @Value("") если названия полей не соответствуют application-cache.properties
    private int clients;
    private int microgreens;
    private int shelvingUnits;
}
